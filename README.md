# cra-template-tailwindcss-typescript

A streamlined [Tailwind CSS](https://tailwindcss.com) template for [Create React App](https://github.com/facebook/create-react-app) in [TypeScript](https://www.typescriptlang.org/).

> This template installs and sets up [Tailwind CSS](https://tailwindcss.com) and [craco](https://github.com/gsoft-inc/craco).

## Usage

```bash
npx create-react-app --template tailwindcss-typescript
```

## Credits

This project was made possible thanks to the following projects.

1. [GeoffSelby/cra-template-tailwindcss](https://github.com/GeoffSelby/cra-template-tailwindcss) - A streamlined Tailwind CSS template for Create React App (in JavaScript).
2. [cra-template-typescript](https://github.com/facebook/create-react-app/tree/master/packages/cra-template-typescript) - An official TypeScript template for create-react-app.

## License

MIT © [Sung M. Kim](https://sung.codes)

# Rhea-Errata

The project's goal is to create a webapp that helps the support teams manage correction processes for all the statements and at the same time avoids the TPA requests, thanks to the S3 Gateway APIs that manages the files in local file-systems.

## Tech Stack

1. Create-React-App with TypeScript & TailwindCSS
1. TailwindCSS is used for styling
1. Micronaut-Kotlin for services, with S3 APIs for managing files

## Basic Flow for Missing TA

1. Upload the missing TA input file using the S3 API to the filesystem
1. Invoke API that will in-turn invoke the NiFi Endpoint API and generate PDF
1. Download and preview the generated PDF
1. If PDF is ok, Approve the generated PDF for Printing & Archival

## Basic Flow for Month End Corrections

TODO

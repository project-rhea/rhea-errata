import React, { useState, Dispatch, SetStateAction, createContext } from "react";
// import airportList from "./data";

interface BU {
    bu: string;
    code: string;
}

let singaporeBU: BU = { bu: "Singapore", code: "0058" }
// let hongkongBU: BU = { bu: "HongKong", code: "0070" }

type Props = {
    children: React.ReactNode;
};

type BUContextType = {
    bu: BU;
    setBU: (value: BU) => void;
};

const initialBUContext: BUContextType = {
    bu: singaporeBU,
    setBU: (value: BU): void => {
        alert('Changing BU...');
        // throw new Error('setContext function must be overridden');
    }
};

const BUContext = createContext<BUContextType>(initialBUContext);

const BUContextProvider = ({ children }: Props): JSX.Element => {

    let [bu, setBU] = useState(singaporeBU);

    return (
        <BUContext.Provider value={{ bu, setBU }}>{children}</BUContext.Provider>
    );
};

export { BUContext, BUContextProvider };
export type { BU };



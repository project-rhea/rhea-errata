import React, { Context } from 'react'
import { BU, BUContext } from '../../context/buContext';

interface ButtonProps {
    twOverrides?: string;
    label: string;
}

type InputEvent = React.MouseEvent<HTMLButtonElement, MouseEvent>;

const Button: React.FC<ButtonProps> = ({ label, twOverrides }) => {

    let hongkongBU: BU = { bu: "HongKong", code: "0070" }
    let singaporeBU: BU = { bu: "Singapore", code: "0058" }

    const [isOn, setIsOn] = React.useState<boolean>(false)

    let { bu, setBU } = React.useContext(BUContext);

    // setBU = (): void => {
    //     // (value.code == '0070') ? value.bu = singaporeBU.bu : value.bu = hongkongBU.bu;
    //     // alert('Changing BU to HK');
    // };

    function showAlert(e: InputEvent) {
        alert("I am an alert box!");
    }
    // console.log(overrides)
    // Use of JS tempate literals to add overrrides to default styles with TailwindCSS 
    // setNewBU(`${bu.code}`)
    // (bu.code == '0058') ? setBU(hongkongBU) : setBU(singaporeBU)
    return (
        <button type="button" className={`${twOverrides + ' inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'}`} onClick={() => (bu.code == '0058') ? setBU(hongkongBU) : setBU(singaporeBU)}>
            {/* {label + ' ' + bu.bu + ' ' + bu.code} */}
            {label + ' ' + bu.bu + ' ' + bu.code}
        </button>
    )
}

export default Button
import React from "react";

/**
 * Component to handle file upload. Works for image
 * uploads, but can be edited to work for any file.
 */
interface UploadSingleFileProps {
    twOverrides?: string;
    label: string;
}

type InputEvent = React.ChangeEvent<HTMLInputElement>;

const UploadSingleFile: React.FC<UploadSingleFileProps> = (props) => {
    // State to store uploaded file
    const [file, setFile] = React.useState<File | undefined>(undefined);

    // Handles file upload event and updates state
    function handleUpload(event: InputEvent) {

        const taFile = event.target.files?.[0];

        if (taFile === undefined)
            console.log(taFile + ' == undefined');
        else
            setFile(taFile);

        // Add code here to upload file to server
        // ...
    }

    return (
        // <div id="upload-box">
        //   <input type="file" onChange={handleUpload} />
        //   <p>Filename: {file?.name}</p>
        //   <p>File type: {file?.type}</p>
        //   <p>File size: {file?.size} bytes</p>
        //   {/* {file && <ImageThumb image={file} />} */}
        // </div>
        <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
            <label htmlFor="cover_photo" className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                {props.label}
            </label>
            <div className="mt-2 sm:mt-0 sm:col-span-2">
                <div className="max-w-lg flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                    <div className="space-y-1 text-center">
                        <svg className="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                            <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                        <div className="flex text-sm text-gray-600">
                            <label htmlFor="file-upload" className="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                <span>Upload a file</span>
                                <input id="file-upload" name="file-upload" type="file" className="sr-only" />
                            </label>
                            <p className="pl-1">or drag and drop</p>
                        </div>
                        <p className="text-xs text-gray-500">
                            PNG, JPG, GIF up to 10MB
          </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

/**
 * Component to display thumbnail of image.
 */
// const ImageThumb = ({ image }) => {
//   return <img src={URL.createObjectURL(image)} alt={image.name} />;
// };

export default UploadSingleFile
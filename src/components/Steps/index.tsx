import React from 'react'

interface StepsProps {
    stepsCount: number;
    stepsHeadline: string[];
    stepsText: string[];
}

const Steps: React.FC<StepsProps> = (props) => {
    const lastIndex = props.stepsCount - 1;
    return (
        // <!-- This example requires Tailwind CSS v2.0+ -->
        <nav aria-label="Progress">
            <ol className="overflow-hidden">
                <li className="relative pb-10">
                    {/* <!-- Complete Step --> */}
                    <div className="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                    <a href="#" className="relative flex items-start group">
                        <span className="h-9 flex items-center">
                            <span className="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                {/* <!-- Heroicon name: check --> */}
                                <svg className="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>
                        <span className="ml-4 min-w-0 flex flex-col">
                            <span className="text-xs font-semibold uppercase tracking-wide">Upload File</span>
                            <span className="text-sm text-gray-500">Vitae sed mi luctus laoreet.</span>
                        </span>
                    </a>
                </li>

                <li className="relative pb-10">
                    {/* <!-- Current Step --> */}
                    <div className="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                    <a href="#" className="relative flex items-start group" aria-current="step">
                        <span className="h-9 flex items-center" aria-hidden="true">
                            <span className="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-indigo-600 rounded-full">
                                <span className="h-2.5 w-2.5 bg-indigo-600 rounded-full"></span>
                            </span>
                        </span>
                        <span className="ml-4 min-w-0 flex flex-col">
                            <span className="text-xs font-semibold uppercase tracking-wide text-indigo-600">Generate PDF</span>
                            <span className="text-sm text-gray-500">Generate PDF using Assentis DocFamily.</span>
                        </span>
                    </a>
                </li>

                <li className="relative pb-10">
                    <div className="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                    <a href="#" className="relative flex items-start group">
                        <span className="h-9 flex items-center" aria-hidden="true">
                            <span className="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                <span className="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                            </span>
                        </span>
                        <span className="ml-4 min-w-0 flex flex-col">
                            <span className="text-xs font-semibold uppercase tracking-wide text-gray-500">Review PDF</span>
                            <span className="text-sm text-gray-500">Review Generated PDF.</span>
                        </span>
                    </a>
                </li>

                <li className="relative pb-10">
                    <div className="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                    <a href="#" className="relative flex items-start group">
                        <span className="h-9 flex items-center" aria-hidden="true">
                            <span className="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                <span className="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                            </span>
                        </span>
                        <span className="ml-4 min-w-0 flex flex-col">
                            <span className="text-xs font-semibold uppercase tracking-wide text-gray-500">Print and Archive</span>
                            <span className="text-sm text-gray-500">Approve PDF for Printing and Archival.</span>
                        </span>
                    </a>
                </li>

                {props.stepsHeadline.map((step, index) => {
                    if (index === lastIndex) {
                        return <li className="relative">
                            <a href="#" className="relative flex items-start group">
                                <span className="h-9 flex items-center" aria-hidden="true">
                                    <span className="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                        <span className="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                    </span>
                                </span>
                                <span className="ml-4 min-w-0 flex flex-col">
                                    <span className="text-xs font-semibold uppercase tracking-wide text-gray-500">Create Audit Log</span>
                                    <span className="text-sm text-gray-500">Add Audit Log.</span>
                                </span>
                            </a>
                        </li>
                    } else {
                        return <li className="relative pb-10">
                            <div className="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                            <a href="#" className="relative flex items-start group">
                                <span className="h-9 flex items-center" aria-hidden="true">
                                    <span className="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                        <span className="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                    </span>
                                </span>
                                <span className="ml-4 min-w-0 flex flex-col">
                                    <span className="text-xs font-semibold uppercase tracking-wide text-gray-500">{step}</span>
                                    <span className="text-sm text-gray-500">{props.stepsText[index] + '.'}</span>
                                </span>
                            </a>
                        </li>
                    }
                })}

            </ol>
        </nav>
    );
}

export default Steps
import React from 'react'
import Button from '../Button'

interface ActionPanelProps {
    header: string;
    bodyText: string;
    buttonLabel: string;
}

const ActionPanel: React.FC<ActionPanelProps> = (props) => {

    return (
        // <!-- This example requires Tailwind CSS v2.0+ -->
        <div className="bg-white shadow sm:rounded-lg">
        <div className="px-4 py-5 sm:p-6">
            <h3 className="text-lg leading-6 font-medium text-gray-900">
            {props.header}
            </h3>
            <div className="mt-2 sm:flex sm:items-start sm:justify-between">
            <div className="max-w-xl text-sm text-gray-500">
                <p>
                {props.bodyText}
                </p>
            </div>
            <div className="mt-5 sm:mt-0 sm:ml-6 sm:flex-shrink-0 sm:flex sm:items-center">
                <button type="button" className="inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                {props.buttonLabel}
                </button>
                {/* <Button label="Upload" overrides="px-4 py-2"/> */}
            </div>
            </div>
        </div>
        </div>
    );
}

export default ActionPanel
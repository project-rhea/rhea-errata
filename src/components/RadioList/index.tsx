import React, { useState, Context } from "react";
import { BU, BUContext } from "../../context/buContext";

const RadioList: React.FC = (props) => {

    let hongkongBU: BU = { bu: "HongKong", code: "0070" }
    let singaporeBU: BU = { bu: "Singapore", code: "0058" }
    let australiaBU: BU = { bu: "Australia", code: "0075" }

    const [bizUnit, setBizUnit] = useState('');

    let { bu, setBU } = React.useContext(BUContext);

    return (
        //         < !--
        //         This example requires Tailwind CSS v2.0 +

        //             This example requires some changes to your config:

        //     ```
        //   // tailwind.config.js
        //   module.exports = {
        //     // ...
        //     plugins: [
        //       // ...
        //       require('@tailwindcss/forms'),
        //     ]
        //   }
        //   ```
        //     -->
        <fieldset>
            <legend className="sr-only">BU setting</legend>

            <div className="bg-white rounded-md -space-y-px">
                {/* <!-- On: "bg-indigo-50 border-indigo-200 z-10", Off: "border-gray-200" --> */}
                <div className="relative border rounded-tl-md rounded-tr-md p-4 flex">
                    <div className="flex items-center h-5">
                        <input
                            id="settings-option-0"
                            name="privacy_setting"
                            type="radio"
                            value="0058"
                            className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 cursor-pointer border-gray-300"
                            // {...(bu == '0058') ? 'checked' : ''}
                            onClick={() => setBU(singaporeBU)}
                        />
                    </div>
                    <label htmlFor="settings-option-0" className="ml-3 flex flex-col cursor-pointer">
                        {/* <!-- On: "text-indigo-900", Off: "text-gray-900" --> */}
                        <span className="block text-sm font-medium">Singapore</span>
                        {/* <!-- On: "text-indigo-700", Off: "text-gray-500" --> */}
                        <span className="block text-sm">
                            Main docs for correction flow for SG BU - TA, SIR, PQR, SCA</span>
                    </label>
                </div>

                {/* <!-- On: "bg-indigo-50 border-indigo-200 z-10", Off: "border-gray-200" --> */}
                <div className="relative border border-gray-200 p-4 flex">
                    <div className="flex items-center h-5">
                        <input
                            id="settings-option-1"
                            name="privacy_setting"
                            type="radio"
                            value="0070"
                            className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 cursor-pointer border-gray-300"
                            onClick={() => setBU(hongkongBU)}
                        />
                    </div>
                    <label htmlFor="settings-option-1" className="ml-3 flex flex-col cursor-pointer">
                        {/* <!-- On: "text-indigo-900", Off: "text-gray-900" --> */}
                        <span className="block text-sm font-medium">HongKong</span>
                        {/* <!-- On: "text-indigo-700", Off: "text-gray-500" --> */}
                        <span className="block text-sm">
                            Main docs for correction flow for HK BU - TA, SIR, PQR</span>
                    </label>
                </div>

                {/* <!-- On: "bg-indigo-50 border-indigo-200 z-10", Off: "border-gray-200" --> */}
                <div className="relative border border-gray-200 rounded-bl-md rounded-br-md p-4 flex">
                    <div className="flex items-center h-5">
                        <input
                            id="settings-option-2"
                            name="privacy_setting"
                            type="radio"
                            className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 cursor-pointer border-gray-300"
                            onClick={() => setBU(australiaBU)}
                        />
                    </div>
                    <label
                        htmlFor="settings-option-2"
                        className="ml-3 flex flex-col cursor-pointer"
                    >
                        {/* <!-- On: "text-indigo-900", Off: "text-gray-900" --> */}
                        <span className="block text-sm font-medium">Australia</span>
                        {/* <!-- On: "text-indigo-700", Off: "text-gray-500" --> */}
                        <span className="block text-sm">
                            Main docs for correction flow for HK BU - TA, SIR, Loan Stmt</span>
                    </label>
                </div>
            </div>
        </fieldset>
    );
};

export default RadioList;
